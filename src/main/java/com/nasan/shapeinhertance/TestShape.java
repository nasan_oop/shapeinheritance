/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeinhertance;

/**
 *
 * @author nasan
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        Triangle triangle = new Triangle(4,3);
        Rectangle rectangle = new Rectangle(4,3);
        Square square = new Square(2);
        
        circle.print();
        triangle.print();
        rectangle.print();
        square.print();
        
    }
}
