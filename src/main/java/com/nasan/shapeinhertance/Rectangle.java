/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeinhertance;

/**
 *
 * @author nasan
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        double area;
        if(width == height){
            area = width*width;
        }else{
        area = width*height;
        }
        return area;
    }
    
    @Override
    public void print(){
        System.out.println("Rectacgle: width: "+ this.width+" height: "+this.height+" area = "+calArea());
    }
}
