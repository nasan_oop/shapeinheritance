/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeinhertance;

/**
 *
 * @author nasan
 */
public class Circle extends Shape {

    private double r;
    private static final double pi = 22.0 / 7;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        double area = pi * Math.pow(r, 2);;
        return area;
    }

    @Override
    public void print() {
        System.out.printf("Circle: r: "+ this.r+" area = %.2f",calArea());
        System.out.println("");
    }    
    
}
