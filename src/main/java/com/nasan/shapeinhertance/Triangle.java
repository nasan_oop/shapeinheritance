/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeinhertance;

/**
 *
 * @author nasan
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle(double base ,double height){
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        double area = (base*height)/2;
        return area;
    }
    
    @Override
    public void print(){
        System.out.println("Triangle: base: "+ this.base+" height: "+this.height+" area = "+calArea());
    }
}
