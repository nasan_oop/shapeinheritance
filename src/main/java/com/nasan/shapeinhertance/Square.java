/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.shapeinhertance;

/**
 *
 * @author nasan
 */
public class Square extends Rectangle {

    public Square(double width) {
        super(width,width);
    }
       
    @Override
    public void print(){
        System.out.println("Square: side: "+ this.width+" area = "+calArea());
    }
    
}
